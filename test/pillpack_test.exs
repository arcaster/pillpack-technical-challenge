defmodule PillpackTest do
  use ExUnit.Case
  doctest Pillpack

  # init various things for testing
  # this is only run once before all tests run (not once before each test)
  setup_all do
    parsed_meds = Pillpack.getJson("http://api-sandbox.pillpack.com/medications")
    {:ok, parsed_list} = parsed_meds

    :ets.new(:med_table, [:duplicate_bag, :public, :named_table])
    Enum.reduce(parsed_list, [], fn(x, acc) ->
      :ets.insert(:med_table, {Map.get(x, "rxcui"), Map.get(x, "id"), x})
      {:ok, :ok}
    end)
  end

  test "handle_json" do
    assert Pillpack.handle_json({:ok, %{status_code: 200, body: '{"id" : 123}'}}) == {:ok, %{"id" => 123}}
  end

  test "getJson" do
    assert {:ok, _ } = Pillpack.getJson("http://api-sandbox.pillpack.com/medications")
  end

  test "runner_recursive" do
    s_list = [%{"id" => "564aab773032360003830000", "medication_id" => "564aab6f3032360003040000", "created_at" => "2015-11-17T04:22:15.111Z", "updated_at" => "2015-11-17T04:22:15.111Z" }]
    assert Pillpack.runner(s_list, :med_table, []) == []
  end

  test "runner_non_recursive" do
    assert Pillpack.runner([], :med_table, []) == []
  end

  test "worker_update" do
    # prescription that needs updated
    script_ex_update = %{"id" => "564aab7430323600034f0000", "medication_id" => "564aab6f3032360003050000", "created_at" => "2015-11-17T04:22:12.156Z", "updated_at" => "2015-11-17T04:22:12.156Z" }

    assert {:update, _ } = Pillpack.worker(script_ex_update, :med_table)
  end

  test "worker_good" do
    # prescription that doesn't need update
    script_ex_good =  %{"id" => "564aab773032360003830000", "medication_id" => "564aab6f3032360003040000", "created_at" => "2015-11-17T04:22:15.111Z", "updated_at" => "2015-11-17T04:22:15.111Z" }

    assert {:good} = Pillpack.worker(script_ex_good, :med_table)
  end
end
