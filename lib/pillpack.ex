defmodule Pillpack do
  @moduledoc """
  Documentation for Pillpack challenge.
  """

  @doc """
  Parse resulting JSON from GET request if status code 200

  Returns:
  List of Map
  """
  def handle_json({:ok, %{status_code: 200, body: body}}) do
    {:ok, Poison.Parser.parse!(body, %{})}
  end

  @doc """
  Makes get request for input url and returns parsed json
  
  Returns:
  List of Map
  """
  def getJson(inp_url) do
    url = inp_url
    |> HTTPoison.get
    |> handle_json
  end

  @doc """
  runner terminating case

  Returns:
  Result of running (acc)
  """
  def runner([],table,acc) do
    acc
  end

  @doc """
  runner recursive case

  Returns:
  recursive runner with incremented inp list
  """
  def runner([head | tail],table,acc) do
    worker(head,table)
    |> case do
        # update needed
        {:update, new_id} -> 
          runner(tail, table, [%{"prescription_id"=>head|>Map.get("id"), "med_subst"=>new_id} | acc])
        # no update needed  
        {:good} -> 
          runner(tail, table, acc)
        # else
        _ -> 
          IO.puts "Something went wrong here"
      end
  end

  @doc """
  Check if given prescription needs to be updated or not

  Returns:
  {:good} | {:update, new_id}
  """
  def worker(inp, table) do
    # input medication id
    orig_med_id = Map.get(inp, "medication_id")
    # medication obj matched to origin_med_id
    orig_rxcui_obj = :ets.match_object(table, {:"$1", orig_med_id, :"$3"})|> hd
    # input rxcui
    orig_rxcui = orig_rxcui_obj|> elem(0)
    # input rxcui generic field
    orig_rxcui_gen = orig_rxcui_obj|> elem(2)|> Map.get("generic")

    # check if dupes of rxcui found
    # returns list
    rxcui_dups = :ets.lookup(table, orig_rxcui)
    cond do
      # no dupes found
      length(rxcui_dups) == 1 ->
        {:good}
      # dupes found
      length(rxcui_dups) > 1 ->
        case orig_rxcui_gen do
          # inp is NOT already generic
          false ->
            res = Enum.filter(rxcui_dups, fn a -> elem(a,2)|> Map.get("generic") end)
            {:update, res|> hd |> elem(2)|> Map.get("id")}
          # inp is a generic... ignore
          true ->
            {:good}
          _ -> 
            IO.puts "something bad happened"
        end

      # not found
      length(rxcui_dups) == 0 ->
        IO.puts "error, no rxcui with med_id #{orig_med_id} found!"
    end
  end

  @doc """
  main event loop
  """
  def main(args) do

    # init ETS lookup table
    IO.puts "Initializing ETS lookup table"
    :ets.new(:med_table, [:duplicate_bag, :public, :named_table])

    # make API call to get list of all medication object
    IO.puts "[API Call] Fetching list of medications"
    parsed_meds = getJson("http://api-sandbox.pillpack.com/medications")
    {:ok, parsed_list} = parsed_meds

    # populate lookup table with data from api results json
    # set <rxcui> and <id> as potential keys for lookup table
    IO.puts "[ETS] Populating ETS lookup table with medications..."
    Enum.reduce(parsed_list, [], fn(x, acc) ->
      :ets.insert(:med_table, {Map.get(x, "rxcui"), Map.get(x, "id"), x})
    end)

    # make api call to get list of all prescriptions
    # ideally this could be polled into a buffer of some kind
    # but for this exercise is done statically on init
    IO.puts "[API Call] Fetching list of prescriptions"
    raw_scripts = getJson("http://api-sandbox.pillpack.com/prescriptions")
    {:ok, script_list} = raw_scripts

    # run prescriptions through workers and get update result
    IO.puts "Searching for possible updates..."
    result = runner(script_list, :med_table, [])
    
    # parse to json and encode as list of binary in order to write to file
    # UTF8 strings in elixir are represented as streams or lists of binary
    IO.puts "[DONE] Writing #{length(result)} updates to 'updates.json'"
    json_res = Poison.encode!(result)|> IO.iodata_to_binary

    # write to file
    {:ok, file} = File.open "updates.json", [:write]
    IO.binwrite file, json_res
    File.close file

  end
end
