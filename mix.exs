defmodule Pillpack.MixProject do
  use Mix.Project

  def project do
    [
      app: :pillpack,
      version: "0.1.0",
      escript: escript(),
      elixir: "~> 1.7",
      start_permanent: Mix.env() == :prod,
      deps: deps()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger]
    ]
  end

  # allows this script to be compiled as executable
  defp escript do
    [main_module: Pillpack]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:httpoison, "~> 1.4"},
      {:poison, "~> 3.1"}
    ]
  end
end
