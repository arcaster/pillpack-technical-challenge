Pillpack Technical Challenge
---
alex crisara

---

## Installation

**Requires Elixir v 1.7 -- see install instructions [here](https://elixir-lang.org/install.html)**

Once elixir is installed:

```
<clone and navigate to repo root>

mix deps.get
mix test
mix escript.build
```

To execute and produce output json file:
`./pillpack` which will produce `updates.json` which will contain results

---

## Design Choices

I chose to use elixir to solve this problem primarily for it's pattern matching and caching features.

API calls are slow, and since the primary incoming data stream is static (for the context of this exercise - although in theory the list representing incoming prescriptions could also be treated as a buffer) it seemed like a good idea minimize API calls to the medication endpoint and store the "all" request as a lookup table and request the master set of prescriptions once then feed the input as a list of parsed json elements.

Elixir has numerous data structures suitable for use as lookup tables.  I decided to only use structures kept in memory for their speed, which limited me to nested tuples, keyword lists and ETS tables.  Keyword lists and nested tuples are faster than normal list lookups and faster than querying a list of maps, however their keys must be atoms.  Unfortunately, atoms in elixir can't be represented by numbers which is the type representing medication_id and rxcui (the best keys to use in the lookup table).  Therefore I decided to go with ETS tables, a feature passed through from the Erlang VM.  ETS tables are about five times as fast as reading from a keyword list (still in memory).  So I stored the rxcui, medication_id and raw parsed json from the original request in each ETS table entry.  This is also ideal, since any index of a given element in an ETS table can be used as a key for lookup.  In this solution, the worker is synchronous, however if multiple workers were needed to handle a larger load of prescriptions that may need updates an ETS share can support up to 16 individual mutexes for read/write concurrency (both keyword lists and nested tuples are blocking data structures and offer far-less concurrency).

Pattern matching is used to identify if duplicates are present and in the case there's a suitable generic replacement -  issue an update for that prescription id with the necessary substitute prescription id.

The main event loop is a simple recursive function meant to accumulate the list of necessary updates, tail recursion is higly optimized in elixir and the Erlang VM which it compiles to.

## Possible Improvements

* Create another field in the ETS table that's initially set to Null, if a suitable generic replacement is found duplicate work could be avoided by simply writing a known result to the extra field.  Querying the extra field or a key for a lookup could be done in a single operation with pattern matching.

* Implementing parallel workers with using GenServer.

